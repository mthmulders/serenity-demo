# What is this?

Sample project to showcase a few of the Serenity features.

# What does it do?

It runs a few simple feature files against a Dutch webshop where you can order smartphones.

# How to run it?

`mvn clean verify` should do the trick. Afterwards, open target/site/thucydides/index.html with your favorite browser.

# References:

* [Serenity-BDD website](http://www.serenity-bdd.net)