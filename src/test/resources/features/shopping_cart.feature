Feature: Shopping cart

  @wip
  Scenario: Add product from search results
    Given I am on a page with search results
    When I click on a product
    Then it should show me a confirmation that the product is in my cart
    And it should offer me to check-out
    And it should offer me to continue shopping

