Feature: Search for products

  Scenario: Search from homepage
    Given I am on the homepage
    When I search for 'Nexus 5'
    Then it should show me how many products relate to 'Nexus 5'
    And it should show me a list of products that match 'Nexus'

  Scenario: Filter on category
    Given I am on a page with search results
    When I filter on the 'Smartphones' category
    Then it should highlight 'Smartphones' as the selected category
