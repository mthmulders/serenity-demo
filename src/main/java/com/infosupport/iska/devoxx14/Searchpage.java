package com.infosupport.iska.devoxx14;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

import java.util.List;

public class Searchpage extends PageObject {
    public WebElementFacade searchResultsHeader() {
        return $("div.searchpage--header h2");
    }

    public List<WebElementFacade> searchResults() {
        return findAll("div.product-items--item");
    }

    public void filterCategory(final String category) {
        findAll("ul.filters--list li span").stream()
                .filter(e -> e.getText().toUpperCase().contains(category.toUpperCase()))
                .findFirst()
                .get().click();
    }

    public List<WebElementFacade> currentFilters() {
        return findAll("li.filters--item a");
    }
}
