package com.infosupport.iska.devoxx14.steps;

import com.infosupport.iska.devoxx14.Homepage;
import com.infosupport.iska.devoxx14.Searchpage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.core.pages.WebElementFacade;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class SearchpageSteps {
    private static Pattern SEARCH_RESULT_PATTERN =
            Pattern.compile("(\\d{1,3}) resultaten voor '([^\"]*)'", Pattern.CASE_INSENSITIVE);

    private Homepage homepage;
    private Searchpage searchpage;

    @Then("^it should show me how many products relate to '([^\"]*)'$")
    public void it_should_show_me_how_many_products_relate_to(final String input) {
        String searchResultsHeader = searchpage.searchResultsHeader().getText().trim();

        Matcher matcher = SEARCH_RESULT_PATTERN.matcher(searchResultsHeader);
        assertThat(matcher.matches(), is(true));
        assertThat(matcher.group(2), equalToIgnoringCase(input));
    }

    @And("^it should show me a list of products that match '([^\"]*)'$")
    public void it_should_show_me_a_list_of_products_that_match_Nexus_(final String input) throws Throwable {
        List<WebElementFacade> results = searchpage.searchResults();

        results.stream()
               .map(r -> r.getText().toUpperCase())
               .forEach(s -> assertThat(s, containsString(input.toUpperCase())));
    }

    @Given("^I am on a page with search results$")
    public void i_am_on_a_page_with_search_results() throws Throwable {
        homepage.open();
        homepage.searchBar().typeAndEnter("Nexus 5");
    }

    @When("^I filter on the '([^\"]*)' category$")
    public void i_filter_on_category(final String category) throws Throwable {
        searchpage.filterCategory(category);
    }

    @Then("^it should highlight '([^\"]*)' as the selected category$")
    public void it_should_highlight_selected_category(final String category) throws Throwable {
        List<WebElementFacade> currentFilters = searchpage.currentFilters();
        WebElementFacade filter = currentFilters.get(0);
        String filterText = filter.getText().toUpperCase();

        assertThat(filterText, containsString(category.toUpperCase()));
    }

    @When("^I click on a product$")
    public void I_click_on_a_product() throws Throwable {
        List<WebElementFacade> searchResults = searchpage.searchResults();
        WebElementFacade firstResult = searchResults.get(0);
        WebElementFacade shoppingCart = firstResult.findBy("div.product-items--action a");

        shoppingCart.click();
    }
}
