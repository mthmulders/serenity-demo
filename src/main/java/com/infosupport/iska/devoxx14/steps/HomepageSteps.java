package com.infosupport.iska.devoxx14.steps;

import com.infosupport.iska.devoxx14.Homepage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

/**
 * Step definitions for interacting with the homepage.
 */
public class HomepageSteps {
    private Homepage homepage;

    @Given("^I am on the homepage$")
    public void i_am_on_the_homepage() {
        homepage.open();
    }

    @When("^I search for '([^\"]*)'$")
    public void i_search_for(final String input) throws Throwable {
        homepage.searchBar().typeAndEnter(input);
    }
}
