package com.infosupport.iska.devoxx14;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://www.smartphoneshop.nl/")
public class Homepage extends PageObject {
    public WebElementFacade searchBar() {
        return $("#search_query");
    }
}
